package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {

	ArrayList<FridgeItem> items;
	
	public Fridge() {
		items = new ArrayList<>();
	}
	
	@Override
	public int nItemsInFridge() {
		// TODO Auto-generated method stub
		return items.size();
	}

	@Override
	public int totalSize() {
		// TODO Auto-generated method stub
		return 20;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		// TODO Auto-generated method stub
		if (nItemsInFridge() < totalSize()){
			items.add(item);
			return true;
		}
		else return false;
	}

	@Override
	public void takeOut(FridgeItem item) {
		// TODO Auto-generated method stub
		if (nItemsInFridge() != 0) {
			items.remove(items.indexOf(item));
		}
		else {
			throw new NoSuchElementException();
		}
	}

	@Override
	public void emptyFridge() {
		// TODO Auto-generated method stub
		items.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		// TODO Auto-generated method stub
		ArrayList<FridgeItem> expiredFood = new ArrayList<>();
		for (int i=0; i < nItemsInFridge(); i++){
			FridgeItem item = items.get(i);
			if (item.hasExpired()) {
				expiredFood.add(item);
			}
			
		}
		for (FridgeItem expiredItem : expiredFood) {
			items.remove(expiredItem);
		}
		return expiredFood;
	}

}
